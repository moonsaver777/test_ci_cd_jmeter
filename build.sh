#!/bin/bash

JMETER_VERSION="5.5"

docker build  --build-arg JMETER_VERSION=${JMETER_VERSION} -t "test_ci_cd_jmeter/jmeter${JMETER_VERSION}" .
